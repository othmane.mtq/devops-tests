DevOps Tests

Hello candidate !

There are no wrong answers here, we're just trying to guage the type of person you are from a technical standpoint.

To begin, simply Fork this repo, add your work and send us a link to your repo. Git best practices are highly appreciated.

Please use the following folder structure for this:

./

├── 01_automation_test

│   └── <your project>

├── 02_coding_test

│   └── <your project>

├── 03_infrastructure_test

│   └── <your project>

└── README.md

Automation Test

Using a tool of your choice such as Bash, Chef, Ansible or similar automate the installation of a Web Server serving up some static content.

Bonus points for the following:

Using Containers as part of your automation.
Multiple OS
Abstraction / Idempodence
Ansible best pratictes
Infrastructure Test

Build out some basic Infrastructure (EC2, ALB, VPC, SG, ..) in AWS using Terraform using our previous step, that can be used in a repeatable way.

Bonus points for the following:

Clearly explaining why you're doing things a certain way.
Github/Github Lab Ci/CD
Env
Terragrunt
Docker / ECS / EKS
